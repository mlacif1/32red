<?php
require_once "lib/promo_manager.php";
require_once "lib/pagination.php";

//if the user asked to log out then we delete the cookie which contains the previously logged user
if (isset($_GET['logout']) && $_GET['logout'] == 'true')
{
	setcookie('promo_username', null, -1);
	header('Location: main.php');
}

// Set up the PromoManager
$promoMgr = new PromoManager();

//Create a Paginator
$paginator = new Paginator(4, sizeof(PromoManager::$promoData));
$message = '';

// Handle the action, if required
if (!empty($_POST['action'])) {
    if ($_POST['action'] == 'optin') {
        try {
            // This returns `false` if it fails, but
            // we don't need to deal with that; because the
            // status won't be updated in that case
            $promoMgr->optIn();
        }
        catch (Exception $e) {
            // @TODO : Use better exceptions here
            
            if (!$promoMgr->isValidUser()) {
                $message = 'Invalid username';
            }
            else {
                $message = 'Something went wrong';
            }
        }
    }
}

$promoStatus = $promoMgr->getStatus();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Test Promotion #1</title>

    <!--<link href="styles/bootstrap.min.css" rel="stylesheet">-->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link href="styles/main.css" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="fonts/Paname/MyFontsWebfontsKit/MyFontsWebfontsKit.css"/>
	<link rel="stylesheet" type="text/css" href="fonts/Texta/MyFontsWebfontsKit/MyFontsWebfontsKit.css"/>
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
	
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
    <script src="scripts/jquery.min.js"></script>
    <!--<script src="scripts/bootstrap.min.js"></script>-->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

	
	<script src="scripts/main.js"></script>

  </head>
  <body>
  
	<header>
		<div class="container">
			<div class="col-md-4 col-sm-4">
				<img src="images/logo.svg" alt="Logo"/>
			</div>
			
			<div class="col-md-4 col-sm-4 hidden-xs" id="title">
				<span>Promotions</span>
			</div>
			
			<?php
				if (isset($_COOKIE['promo_username']))
				{
					$logoutVisibility = "display:block";
				}
				else
					$logoutVisibility = "display:none";
			?>

			<div class="col-md-4 col-sm-4 hidden-xs" style=<?php echo $logoutVisibility?>>
				<div id="logoutDiv">
					<span class="col-md-6 col-sm-4 hidden-xs" >Welcome: <?php echo $_COOKIE['promo_username']?></span>
					<a class="col-md-6 col-sm-8 hidden-xs btn btn-default opt-in-button" href="main.php?logout=true"  >Log Out</a>
				</div>
			</div>
		</div>
	</header>

	<div class="container" id="mainContent">			
		<div class="row">
			
			<?php

				//displaying the available promotions using a paginator
				$promoManager = PromoManager::$promoData;
				if (isset($_GET['getPage']))
				{
					$currentPage = $_GET['getPage'];
				}else
					$currentPage = 1;
					
				if ((($currentPage - 1) * $paginator->itemsPerPage) + $paginator->itemsPerPage >sizeof(PromoManager::$promoData))
				{
					$size = sizeof(PromoManager::$promoData);
				}
				else
					$size = (($currentPage - 1) * $paginator->itemsPerPage) + $paginator->itemsPerPage;

				for ($i = (($currentPage - 1) * $paginator->itemsPerPage); $i < $size; $i++)
				{
					$id = $promoManager[$i]['id'];
					$description = $promoManager[$i]['description'];
					$title = $promoManager[$i]['title'];
					$image = $promoManager[$i]['image'];
				?>	
					<div class="col-md-6 col-sm-12 col-xs-12 promotion">
						<div class="content">
							<p class="title"><?php echo $title; ?></p>
								<div class=" promotion-text ellipsis">
									<div>
										<p><?php echo $description; ?></p>
									</div>
								</div>										
								
							<img class="promotion-image" src="<?php echo $image;?>" alt="Image"/>
							
						</div>
							
						<a class="btn btn-default opt-in-button" href="promotion.php?id=<?php echo $id ?>"  >View</a>

					</div>
						
				<?php	
				}
			?>
			
		</div>
		
		<?php
			$paginator->paginate();
			print_r($paginator->pagesAsHtml);
		?>
		
	</div>
  </body>
</html>