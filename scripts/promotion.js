$(document).ready(function(){
	$("#infoDiv").hide();
});

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

function optIn()
{
	var action = $("#txtAction").val();
	var username = $("#txtUsername").val();
	var promo = $("#txtPromo").val();

	if (username == "")
		username = getCookie('promo_username');

	$.ajax({
	    type: "GET",  
	    url: "api.php?promo=" + promo + "&action=" + action + "&username=" + username,

	    success: function(data){  
	    	$("#loginDiv").hide();

	    	var spanString = "<span>";

	    	var spanContent = JSON.stringify(data);

	    	if (data['result'] == true )
	    	{
	    		if (data['status'] == "in")
	    		{
	    			//user opted for this promotion
	    			spanContent = "<span  id='usernameSpan' >You (" + username + ")</span>" + " Opted for this promotion.";
	    		}
	    	}
	    	else if (data['status'] == "in")
	    	{
	    		//user already opted for this promotion
	    		spanContent = "<span  id='usernameSpan' >You (" + username + ")</span>" + " Already opted for this promotion.";
	    	}
	    	else if (data['status'] == "not-eligible")
	    	{
	    		//user is not eligible for this promotion
	    		spanContent = "<span  id='usernameSpan' >You (" + username + ")</span>" + " Are not eligible for this promotion.";
	    	}
	    	else if (data['status'] == "in-blacklist")
	    	{
	    		//user is in blacklist
	    		spanContent = "<span  id='usernameSpan' >You (" + username + ")</span>" + " Are in blacklist so cannot opt for promotions.";
	    	}

	    	spanString = spanString + spanContent;
	    	spanString = spanString + "</span>";

	    	$("#infoDiv").append(spanString);
	    	$("#infoDiv").show();

	    },
	    error: function(XMLHttpRequest, textStatus, errorThrown) { 
	    	var spanString = "<span class='error'>";
	    	spanString = spanString + errorThrown;
	    	spanString = spanString + "</span>";

	    	$("#infoDiv").append(spanString);
	    	$("#infoDiv").show();
	    }       
});
}