<?php

$ch = curl_init("http://localhost/32RED/api.php?promo=" . $_GET['id'] . "&action=" . "getPromotion");

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, 0);

$result = curl_exec($ch);
curl_close($ch);


$result = json_decode($result, true);

?>

<html>
	<title>Promotion: </title>

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link href="styles/promotion.css" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="fonts/Paname/MyFontsWebfontsKit/MyFontsWebfontsKit.css"/>
	<link rel="stylesheet" type="text/css" href="fonts/Texta/MyFontsWebfontsKit/MyFontsWebfontsKit.css"/>
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
	
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
    <script src="scripts/jquery.min.js"></script>
    <!--<script src="scripts/bootstrap.min.js"></script>-->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="scripts/promotion.js"></script>

<head>
</head>


<body>

	<header>
		<div class="container">
			<div class="col-md-4 col-sm-4">
				<img src="images/logo.svg" alt="Logo"/>
			</div>
			
			<div class="col-md-4 col-sm-4 hidden-xs" id="title">
				<span>Promotions</span>
			</div>
			

		</div>
	</header>

	<div class="container" id="mainContent">
	
		<div class="content">
			<p class="title"><?php echo $result['result']['title']; ?></p>
							
			<div id="promotionInfo">
				<div class=" promotion-text">
					<p><?php echo $result['result']['description'];  ?></p>
				</div>										
										
				<img class="promotion-image" src="<?php echo $result['result']['image']; ?>" alt="Image"/>
				
			</div>			
		</div>
				
		<?php

			// if no user is logged then we show the username textBox
			if ($result['status'] == "no-user")
				$loginVisibility = "display:block";
			else
				$loginVisibility = "display:none";

			if (isset($_COOKIE['promo_username']))
			{
				$txtUsername = "none";
			}
			else
				$txtUsername = "block";
		?>
				
		<div id="loginDiv" style=<?php echo $loginVisibility?>>

			<p>Opt in to this promotion!</p>
			
			<input type="hidden" name="promo" id="txtPromo" value="<?php echo $result['result']['id']; ?>" />
			<input type="hidden" name="action" id="txtAction" value="optin" />
			<input style="display: <?php echo $txtUsername ?> "type="text" placeholder="Username" id="txtUsername" name="username" />
			<input class="btn btn-default opt-in-button" type="button" value="Opt In" onclick="optIn()" name="btnOptIn"/>

		</div>

		<div id="infoDiv" >

		</div>
				
	</div>

</body>

</html>