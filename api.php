<?php

require_once "lib/promo_manager.php";

header("Content-type: application/json");

// Attempt to set up the promo
if (empty($_GET['username']))
{
    if (isset($_COOKIE['promo_username']))
	   $promoMgr = new PromoManager($_GET['promo'], $_COOKIE['promo_username']);
    else 
        $promoMgr = new PromoManager($_GET['promo'], null);
}
else
{
	$promoMgr = new PromoManager($_GET['promo'], $_GET['username']);
}

// Check that the passed promo is valid
if (!$promoMgr->isValidPromo()) {
    http_response_code(400);
    die(json_encode(array("error" => "Invalid promotion")));
}

// Check that the passed user is valid
if (!empty($_GET['username']))
{
	if (!$promoMgr->isValidUser()) {
		http_response_code(400);
		die(json_encode(array("error" => "Invalid username")));
	}
}

switch($_GET['action']) {
    case 'status':
        die(json_encode(array(
            "status" => $promoMgr->getStatus()
        )));
        break;
    case 'optin':
        die(json_encode(array(
            "result" => $promoMgr->optIn(),
            "status" => $promoMgr->getStatus()
        )));
        break;
	case 'getPromotion':
        die(json_encode(array(
            "result" => $promoMgr->getPromotion(),
            "status" => $promoMgr->getStatus()
        )));
        break;
    default:
        http_response_code(400);
        die(json_encode(array("error" => "Invalid action")));
        break;
}