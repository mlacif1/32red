<?php
require_once "lib/promo_manager.php";

// Set up the PromoManager with our promo code
$promoCode = 'testPromo1';
$promoMgr = new PromoManager($promoCode);

$message = '';

// Handle the action, if required
if (!empty($_POST['action'])) {
    if ($_POST['action'] == 'optin') {
        try {
            // This returns `false` if it fails, but
            // we don't need to deal with that; because the
            // status won't be updated in that case
            $promoMgr->optIn();
        }
        catch (Exception $e) {
            // @TODO : Use better exceptions here
            
            if (!$promoMgr->isValidUser()) {
                $message = 'Invalid username';
            }
            else {
                $message = 'Something went wrong';
            }
        }
    }
}

$promoStatus = $promoMgr->getStatus();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Test Promotion #1</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
        <h1>Something about a promotion</h1>
        
        <p>Bacon ipsum dolor amet brisket aliqua velit anim drumstick fugiat nisi dolor cillum magna filet mignon pastrami tri-tip. Culpa pork pork loin laborum consequat. Dolore excepteur laborum, proident deserunt fugiat enim. Reprehenderit commodo et qui irure labore boudin doner tempor adipisicing cupidatat.</p>

        <p>Spare ribs aliqua alcatra, tongue ut beef ribs exercitation nisi veniam ipsum. Culpa frankfurter ham hock id eiusmod cupim strip steak sint turkey kielbasa labore boudin ground round officia. Aliqua landjaeger qui pork chop, brisket picanha short ribs culpa nisi tongue. Turkey commodo shankle tail, prosciutto kevin excepteur frankfurter deserunt qui pork. Chuck consequat cow, turkey proident nisi strip steak. Venison velit in, landjaeger anim boudin enim tongue ham. Laborum ham hock qui ea boudin, venison nisi irure consequat enim frankfurter.</p>
        
        <?php if (!empty($message)) : ?>
        
        <div class="alert alert-danger" role="alert"><?= $message; ?></div>
        
        <?php endif; ?>
        
        <?php if ($promoStatus === 'error'): ?>
        
        <div class="alert alert-danger" role="alert">Details for this promotion are not available right now.</div>
        
        <?php elseif ($promoStatus === 'in'): ?>
        
        <div class="alert alert-success" role="alert">You've opted in to this promotion!</div>
        
        <?php elseif ($promoStatus === 'eligible' || $promoStatus === 'no-user'): ?>
        
        <form method="post" action="">
            <input type="hidden" name="action" value="optin" />
            <input type="hidden" name="bonus" value="<?= $promoMgr->getPromoKey(); ?>" />
            <div class="form-group">
                <label for="promoFormUsername">Username</label>
                <input id="promoFormUsername" class="form-control" type="input" name="username" value="<?= htmlspecialchars($promoMgr->getUsername()); ?>" />
            </div>
            <button class="btn btn-default" type="submit">Opt me in to this promotion!</button>
        </form>
        
        <?php elseif ($promoStatus === 'not-eligible'): ?>
        
        <div class="alert alert-warning" role="alert">You're not eligible for this promotion. Sucks to be you.</div>
        
        <?php endif; ?>

        <p>Tri-tip chuck tempor sausage, meatball andouille ground round sed jowl officia. Aute pork chop andouille, beef consectetur duis eiusmod velit short loin pork ut tenderloin pig. Flank eiusmod nulla nostrud capicola, in spare ribs eu chicken pariatur qui shank adipisicing minim dolor. Hamburger doner ground round short loin tail picanha id pork loin, eu strip steak exercitation. Ex quis voluptate consectetur hamburger ut sunt sirloin enim ut prosciutto. Proident drumstick tail pastrami in.</p>

        <p>Eu flank excepteur magna, venison kevin pork chop ball tip exercitation ipsum pork dolor chicken. Exercitation fatback meatloaf, velit pork cupidatat sirloin ham. Biltong lorem esse irure jowl. Velit ut ad cow, adipisicing ex short loin est cupidatat landjaeger pancetta jerky mollit tail turkey.</p>

        <p>Elit officia mollit adipisicing exercitation ut, pancetta do et. Tenderloin enim minim officia in. Cupim culpa adipisicing tempor bresaola ball tip esse ullamco. Sint laborum ad pastrami ex est.</p>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>