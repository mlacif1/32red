<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'player.php';

class PromoManager {
    
	 /**
     * Path of file used for storing players and their tags
     * Placeholder functionality only
     *
     * @var string
     */
    private static $dataFileName;
   
	
    /**
     * Stores the promo config - key and OptIn tag
     * Loaded from promotions.json file
     * 
     * @var array Promotions data
     */
	 
    static $promoData = array();
    
    /**
     * Key of current promotion
     *
     * @var string
     */
    private $promoKey = null;
    
    /**
     * Current username
     *
     * @var string
     */
    private $username = null;
    
    /**
     * Current player object
     *
     * @var Player
     */
    private $player = null;
    
	public static function init() {
        self::$dataFileName = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'promotions.json';
		
        //Every promotion is uniquely identified using an 'id'
		self::$promoData = json_decode(file_get_contents(self::$dataFileName), true);
    }
	
    /**
     * Constructor
     * 
     * @param string promo key to use
     * @param mixed username to load; set to "true" (boolean) for auto-detection
     */
    public function __construct($promoKey = null, $username = true) {
        if (!empty($promoKey)) {
            $this->promoKey = $promoKey;
        }

        if (!empty($username)) 
        {

            if (!empty($_GET['username'])) {
                $this->setUsername($_GET['username']);
                setcookie('promo_username', $this->username, time()+60*60*24);
            }
            elseif (!empty($_GET['user'])) {
                $this->setUsername($_GET['user']);
                setcookie('promo_username', $this->username, time()+60*60*24);
            }
            elseif (!empty($_COOKIE['promo_username'])) {
                $this->setUsername($_COOKIE['promo_username']);
            }
            else {
                // No auto username found
                $this->setUsername('');
            }
        }
    }
    
    /**
     * Returns the current player object
     * 
     * @return Player current player object
     */
    public function getPlayer() {
        return $this->player;
    }
    
    /**
     * Returns the current username
     * 
     * @return string current username
     */
    public function getUsername() {
        return $this->username;
    }
    
    /**
     * Sets the current username
     * This initializes the Player object
     */
    public function setUsername($value) {
        $this->username = $value;
        $this->player = new Player($this->username);
    }
    
    /**
     * Returns the current promotion key
     * 
     * @return string current promotion key
     */
    public function getPromoKey() {
        return $this->promoKey;
    }
    
    /**
     * Sets the current promotion key
     */
    public function setPromoKey($value) {
        $this->promoKey = $value;
    }
    
    /**
     * Helper function to ensure we have valid settings
     * 
     * @throws Exception if the settings aren't valid
     */
    private function requireValidSettings() {
        if (!$this->isValidUser()) {
            throw new Exception('Valid Username not set');
        }
        if (!$this->isValidPromo()) {
            throw new Exception('Valid promo not set');
        }
    }
    
    /**
     * Checks if the current user is valid
     * 
     * @return boolean
     */
    public function isValidUser() {
        if (empty($this->username) || !$this->player->userNameIsValid()) {
            return false;
        }
		
		return true;
    }
    
    /**
     * Checks if the current promo is valid
     * 
     * @return boolean
     */
    public function isValidPromo() {

        if (empty($this->promoKey)) {
            return false;
        }
		
		foreach (self::$promoData as $promotion)
		{
			if ($promotion['id'] == $this->promoKey)
				return true;
		}

        return false;
    }
    
    /**
     * Checks if the current user is opted in to the current promo
     * 
     * @return boolean
     */
    public function isOptedIn() {
        $this->requireValidSettings();

        return $this->player->hasTag($this->promoKey);
    }
    
	 /**
     * Checks if the current user cannot be opted in to the current promo
     * 
     * @return boolean
     */
    public function hasNotEligibleTag() {
        $this->requireValidSettings();
        if ($this->player->hasNotEligibleTag($this->promoKey) == 'true') {

            return 'true';
        } 
        return 'false';
    }
	
    /**
     * Checks if the current user is eligible for the current promo
     * For now just checks if the user is valid and not opted-in already
     * but this functionality could be extended.
     * 
     * @return boolean
     */
    public function isEligible() {
        $this->requireValidSettings();

        if ($this->isInBlackList() == 'true'){
            return 'false';
        }

        if ($this->isOptedIn() == 'true')
        {
            return 'false';
        }

        if ($this->hasNotEligibleTag() == 'true') 
        {
            return 'false';
        }

        return 'true';
    }
    
	 /**
     * Checks if the current user is in blacklist 
     * @return boolean
     */
    public function isInBlackList() {
        $this->requireValidSettings();
        return $this->player->getInBlackList();
    }
	
    /**
     * Returns the promotion status for the current user in the current promo
     * 
     * @return string
     */
    public function getStatus() {
        
        if (!$this->isValidPromo()) {
            return 'error';
        }
        
        if (!$this->isValidUser()) {
            return 'no-user';
        }
        
        if ($this->isOptedIn() == 'true') {
            return 'in';
        }
		
        if ($this->isEligible() ==  "true") {
            return 'eligible';
        }
		
		if ($this->isInBlackList() == "true"){
			return 'in-blacklist';
		}

        return 'not-eligible';
    }
    
    /**
     * Opts the current player in to the current promo
     * 
     * @return boolean success state
     */
    public function optIn() {
        $this->requireValidSettings();

        if ($this->isEligible() == 'false') {
            return false;
        }
        $this->player->addTag($this->promoKey);
        
        return true;
    }
    
	public function getPromotion()
	{
		foreach (self::$promoData as $promotion)
		{
			if ($promotion['id'] == $this->promoKey)
			{
				return $promotion;
			}
		}
	}
}

PromoManager::init();