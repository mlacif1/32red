<?php

class Paginator
{
	public $currentPage;
	public $itemsPerPage;
	public $totalPageNumber;
	public $currentLink;
	public $pagesAsHtml;
	
	public function __construct($itemsPerOnePage, $totalItems){
		$this->currentPage = 1;
		$this->itemsPerPage = $itemsPerOnePage;
		$this->totalPageNumber = $totalItems;
		$this->currentLink = $_SERVER['PHP_SELF'];
	}
	
	public function paginate()
	{
		if (isset($_GET['current']))
		{
			$this->currentPage = $_GET['getPage'];
		}
		
		if (isset($_GET['item']))
		{
			$this->itemsPerPage = $_GET['item'];
		}
		
		$this->pagesAsHtml = $this->getPages();
	}
	
	public function getPages()
	{
		$html = '<ul class="pagination">';
		
		for ($i = 1; $i <= ceil($this->totalPageNumber / $this->itemsPerPage); ++$i){
			$html = $html . '<li>';
			$html = $html . '<a href="' . $this->currentLink . '?getPage=' . $i . '">' . $i .'</a>';
			$html = $html . '</li>';
		}
		
		$html = $html . '</ul>';
		return $html;
	}
}


?>