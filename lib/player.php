<?php

class Player {

    /**
     * Path of file used for storing players and their tags
     * Placeholder functionality only
     *
     * @var string
     */
    private static $dataFileName;
    
    /**
     * Current username
     *
     * @var string
     */
    private $username = null;
    
    /**
     * Is the current user valid?
     *
     * @var boolean
     */
    private $valid = false;
    
    /**
     * Current user's tags
     *
     * @var array
     */
    private $tags = array();
    
	/**
	* Eligible tags, defines those promotions to which the user can opt in_array
	*
	* @var array
	*/
	private $notEligibleTags = array();
	
	
	/*
	* Playe is in black list
	*/
	private $inBlackList = false;
	
    /**
     * Initialize static variables
     */
     
    public static function init() {
        self::$dataFileName = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'playertags.json';
    }
    
    /**
     * Constructor
     * 
     * @param string username
     */
    public function __construct($username = null) {
        if ($username !== null) {
            $this->setUsername($username);
        }
    }
    
    /**
     * Sets the current username
     * This initializes the Player object
     */
    public function setUsername($value) {
        $this->username = $value;
        $this->loadTags();
    }
	
	/**
     * Sets the current in black list
     * This initializes the Player is in blac list
     */
    public function setInBlackList($value) {
        $this->inBlackList = $value;
    }
    
    
    /**
     * Returns the current username
     * 
     * @return string current username
     */
    public function getUsername() {
        return $this->username;
    }
	
	/**
     * Returns the current is in black list
     * 
     * @return bool inBlackList
     */
    public function getInBlackList() {
        return $this->inBlackList;
    }
    
    /**
     * Returns a boolean value indicating if the current user is valid
     * 
     * @return boolean
     */
    public function isValid() {
        return $this->valid;
    }
    
    /**
     * Gets an array of all of the current user's tags
     * 
     * @return array
     */
    public function getTags() {
        return $this->tags;
    }
	
	/**
     * Gets an array of all of the current user's not eligible tags
     * 
     * @return array
     */
    public function getNotEligibleTags() {
        return $this->notEligibleTags;
    }
    
    /**
     * Allows checking if the current user has a specific tag
     * 
     * @param string $tag The tag to check
     * @return boolean
     */
    public function hasTag($tag) {
         if (in_array((string)$tag, $this->tags, true)) {
            return 'true';
         }
         return 'false';
    }
    
	/**
     * Allows checking if the current user has a specific tag to which the user cannot opt in
     * 
     * @param string $tag The tag to check
     * @return boolean
     */
    public function hasNotEligibleTag($tag) {
        if (in_array($tag, $this->notEligibleTags)) {
            return 'true';
        }
        return 'false';
    }
	
    /**
     * Adds the specified tag to the current user
     * 
     * @param string $tag The tag to add
     * @return boolean
     */
    public function addTag($tag) {

        if ($this->hasTag($tag) == 'false') {
            $this->tags[] = $tag;
            $this->saveTags();
        }
    }
    
    
    /**
     * Loads all players, tags and eligibleTags from storage
     * (Placeholder functionality only)
     */
    private function loadTags() {
        $players = json_decode(file_get_contents(self::$dataFileName), true);
        if (array_key_exists($this->username, $players)) {
			
            $this->valid = true;
		
            $this->notEligibleTags = $players[$this->username]['cannotoptin'];
			
			$this->tags = $players[$this->username]['isoptin'];

			$this->inBlackList = $players[$this->username]['inblacklist'];

        }
        else {
            $this->valid = false;
			$this->notEligibleTags = array();
            $this->tags = array();
        }
    }

	/**
    *Checks  if the current username exists in our 'database' (in this example we are  using .json file)
    */
	public function userNameIsValid(){
		$players = json_decode(file_get_contents(self::$dataFileName), true);
		if (array_key_exists($this->username, $players)) {
            return true;
        }

		return false;
        
	}
    
    /**
     * Saves all players and tags to storage
     * (Placeholder functionality only)
     */
    private function saveTags() {
        $players = json_decode(file_get_contents(self::$dataFileName), true);
        $players[$this->username]['isoptin'] = $this->tags;
        file_put_contents(self::$dataFileName, json_encode($players));
    }
}

Player::init();